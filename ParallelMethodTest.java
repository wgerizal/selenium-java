package newproject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.testng.annotations.Test;
public class ParallelMethodTest {
	public static WebDriver driver;
	@Test(invocationCount = 1, threadPoolSize = 1)
	 public static void main(String[] args) throws InterruptedException{
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--use-fake-device-for-media-stream");
		options.addArguments("use-fake-ui-for-media-stream");
		System.setProperty("webdriver.chrome.drive", "C://chromedriver.exe");
		System.setProperty("webdriver.chrome.logfile", "C://Users//Public//chromedriver10.log");
		System.setProperty("webdriver.chrome.verboseLogging", "true");
		driver = new ChromeDriver(options);
		driver.manage().window().setSize(new Dimension(800,600));
		driver.get("https://beta.astrnt.co/selenium/bcg/astronaut");
		boolean result_start_stress_test = false;
		while(result_start_stress_test ==false) {
			 try {
				 	driver.findElement(By.id("start-stress-test")).click();
				 	result_start_stress_test = true;
		            break;
		        } catch(Exception  e) {
		        	result_start_stress_test = false;
		        }
			 
			
			Thread.sleep(1000);
		}
		
		boolean result_button_impress = false;
		while(result_button_impress ==false) {
			 try {
				 driver.findElement(By.className("button-impress")).click();
				 result_button_impress = true;
		            break;
		        } catch(Exception  e) {
		        	result_button_impress = false;
		        }
			Thread.sleep(1000);
		}
		
		boolean result_circularprogressbar_text = false;
		while(result_circularprogressbar_text ==false) {
			 try {
				 driver.findElement(By.className("CircularProgressbar-text")).click();
				 result_circularprogressbar_text = true;
		            break;
		        } catch(Exception  e) {
		        	result_circularprogressbar_text = false;
		        }
			Thread.sleep(1000);
		}
		
		boolean result_start_now = false;
		while(result_start_now ==false) {
			 try {
				 driver.findElement(By.className("button-start-now")).click();
				 result_start_now = true;
		            break;
		        } catch(Exception  e) {
		        	result_start_now = false;
		        }
			Thread.sleep(1000);
		}
		Thread.sleep(20000);
		boolean result_circularprogressbar_text_2 = false;
		while(result_circularprogressbar_text_2 ==false) {
			 try {
				 driver.findElement(By.className("CircularProgressbar-text")).click();
				 result_circularprogressbar_text_2 = true;
		            break;
		        } catch(Exception  e) {
		        	result_circularprogressbar_text_2 = false;
		        }
			Thread.sleep(1000);
		}
		
		boolean result_checklist = false;
		while(result_checklist ==false) {
			 try {
				 driver.findElement(By.className("btn-checklist")).click();
				 result_checklist = true;
		            break;
		        } catch(Exception  e) {
		        	result_checklist = false;
		        }
			Thread.sleep(1000);
		}
		
		boolean result_primary = false;
		while(result_primary ==false) {
			 try {
				 driver.findElement(By.className("button__primary")).click();
				 result_primary = true;
		            break;
		        } catch(Exception  e) {
		        	result_primary = false;
		        }
			Thread.sleep(1000);
		}

		driver.quit();
		 
	}
	
}

